﻿using System;
using System.Windows;
using System.Windows.Input;

namespace TetrisMVC

{
    public partial class MainWindow
    {
        readonly Controller _control = new Controller();
        readonly Model _model = new Model();
        private readonly System.Windows.Threading.DispatcherTimer _dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();
            _control.Startup(McGrid, _model);
            _model.Startup(McGrid);
            _model.RandomPiece();
            _control.SetIni();
            _control.SetScore();
        }
        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (_control.Stop == 0) return;
            switch (e.Key)
            {
                case Key.Down:
                    if (_control.CanMoveDown(_model.Temp))
                    {
                        _control.Movedown(_model.Temp);
                    }
                    else
                    {
                        _control.UpdateGameBoard(_model.Temp);
                        _control.CheckRow();
                        _model.RandomPiece();
                    }
                    break;
                case Key.Left:
                    if (_control.CanMoveLeft(_model.Temp))
                    {
                        _control.MoveLeft(_model.Temp);
                    }
                    break;
                case Key.Right:                
                    if (_control.CanMoveRight(_model.Temp))
                    {
                        _control.MoveRight(_model.Temp);
                    }
                    break;
                case Key.Up:
                    _control.Rotate(_model.Temp);
                    break;
                case Key.RightShift:
                    _control.FastDown();
                    break;
                case Key.Escape:
                    Application.Current.Shutdown();
                    break;
            }
        }
        public void Start(object sender, RoutedEventArgs e)
        {
            if (_control.Stop != 0) return;
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
            _dispatcherTimer.Tick += Loop;/*game loop*/
            _dispatcherTimer.Start();
            _control.Stop++;
        }
        public void Stop(object sender, RoutedEventArgs e)
        {
            _dispatcherTimer.Tick -= Loop;
            _control.Stop = 0;
        }
        public void Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }  
        public void Returnbase()
        {
            _dispatcherTimer.Tick -= Loop;
            _control.CreateGameBoard();
            _model.RandomPiece();
            _control.SetScore();
            _control.SetIni();
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
            _dispatcherTimer.Tick += Loop;
            _dispatcherTimer.Start();
            Loop(null, null);
            _control.Stop = 1;
        }
        public void NewGame(object sender, RoutedEventArgs e)
        {
            _control.Score = 0;
            Returnbase();
        }
        public void Loop(object sender, EventArgs e)
        {
            _control.CheckRow();
            if (_control.CanMoveDown(_model.Temp))
            {
                _control.CheckRow();
                _control.Movedown(_model.Temp);
                _control.CheckRow();
                _control.SetScore();
            }
            else
            {
                _control.UpdateGameBoard(_model.Temp);
                _control.CheckRow();
                _model.RandomPiece();
                _control.CheckRow();
                _control.SetScore();
            }
            if (_control.IsgameEnd())
            {
                MessageBox.Show("Score: " + _control.Score);
                Stop(null, null);
                NewGame(null, null);
            }
            if (_control.Score >= 1000)
            {
                _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 600);
                _control.Scoreboost = 10;
            }
            if (_control.Score >= 3000)
            {
                _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 400);
                _control.Scoreboost = 15;
            }
            if (_control.Score < 8000) return;
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            _control.Scoreboost = 20;
        }
    }

}

